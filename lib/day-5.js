exports.part1 = function ( vents ) {
    let floor = [];

    vents
        .filter( vent => axial( vent ) )
        .forEach(
            vent => {
                steps( ...vent ).forEach(
                    ( [ x, y ] ) => {
                        if( ! defined( floor[x] ) ) {
                            floor[x] = [];
                        }

                        floor[x][y] = ( floor[x][y] || 0 ) + 1;
                    }
                );
            }
        );

    return flat( floor ).filter(  a =>  a > 1  ).length;
}

exports.part2 = function ( vents ) {
    let floor = [];

    vents
        .forEach(
            vent => {
                steps( ...vent ).forEach(
                    ( [ x, y ] ) => {
                        if( ! defined( floor[x] ) ) {
                            floor[x] = [];
                        }

                        floor[x][y] = ( floor[x][y] || 0 ) + 1;
                    }
                );
            }
        );

    return flat( floor ).filter(  a =>  a > 1  ).length;
}


function axial( [ from, to ] ) {
    return from[0] == to[0]  ||  from[1] == to[1];
}


function defined( x ) {
    return typeof x !== typeof undefined;
}


function steps( [ x1, y1 ], [ x2, y2 ] ) {
    const route = [ [x1, y1 ] ];

    while ( x1 != x2 || y1 != y2 ) {
        x1 += towards( x1, x2 );
        y1 += towards( y1, y2 );
        route.push( [ x1, y1 ] );
    }

    return route;
}


function towards( a, b ) {
    
    return  a < b  ?  1
        : a > b ? -1
        : 0;
}


function flat( arr ) {
    return [].concat(...arr);
}
