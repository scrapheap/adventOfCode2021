const { sum0, defined } = require( '../lib/utils.js' );

exports.part1 = function ( crabs ) {
    const lowestPos = Math.min( ...crabs );
    const highestPos = Math.max( ...crabs );

    let fuel = undefined;
    for( let pos = lowestPos; pos <= highestPos; pos++ ) {
        posFuel = sum0(
            crabs.map( v => Math.abs( v - pos ) )
        );

        fuel = defined( fuel ) ? Math.min( fuel, posFuel ) : posFuel;
    }

    return fuel;
}

exports.part2 = function ( crabs ) {
    const lowestPos = Math.min( ...crabs );
    const highestPos = Math.max( ...crabs );

    const fuelCost = [ 0 ];
    for( let pos = 1; pos <= highestPos - lowestPos; pos++ ){
        fuelCost[ pos ] = fuelCost[ pos -1 ] + pos;
    }
    
    let fuel = undefined;
    for( let pos = lowestPos; pos <= highestPos; pos++ ) {
        posFuel = sum0(
            crabs.map( v => fuelCost[ Math.abs( v - pos ) ] )
        );

        fuel = defined( fuel ) ? Math.min( fuel, posFuel ) : posFuel;
    }

    return fuel;
}
