exports.part1 = function ( bingo ) {
    let numbers = bingo.numbers.map( x => x );

    let boards = bingo.boards.map( board );
    let winningBoard = undefined;

    let number = undefined;

    while( ! defined(winningBoard) ) {
        number = numbers.shift();

        boards.forEach( board => board.markNumber( number ) );

        winningBoard = boards.reduce(
            ( winner, board ) => {
                return defined( winner )
                    ? winner
                    : board.hasWon()
                        ? board
                        : undefined
            },
            undefined
        );
    }

    return winningBoard.value() * number;
}

exports.part2 = function ( bingo ) {
    let numbers = bingo.numbers.map( x=> x );

    let boards = bingo.boards.map( board );
    let winningBoard = undefined;

    let number = undefined;
    let winningBoards = [];

    while( boards.length ) {
        number = numbers.shift();

        boards.forEach( board => board.markNumber( number ) );

        winningBoards.push( ...boards.filter( board => board.hasWon() ) );

        boards = boards.filter( board => ! board.hasWon() );
    }

    const lastWinningBoard = winningBoards.pop();

    return lastWinningBoard.value() * number;
}

function board( srcBoard ) {
    let board = cloneBoard( srcBoard );

    return {
        log: function () {
            console.log( board );
        },

        markNumber: function ( number ) {
            board = board.map(
                row => row.map(
                    value  =>  value == number  ?  undefined  :  value
                )
            );
        },

        value: function () {
            return sum0( board.map( row => sum0(row) ) );
        },

        columns: function () {
            const columns = [];

            board.forEach(
                row => {
                    let pos = 0;
                    row.forEach(
                        value => {
                            if ( ! defined( columns[ pos ] ) ) {
                                columns[ pos ] = [];
                            }
                            columns[ pos++ ].push( value );
                        }
                    );
                }
            );

            return columns;
        },

        hasWon: function ( ) {
            const horizontalWin = board.reduce(
                ( win, row ) => win || sum0( row ) == 0,
                false
            );

            const verticalWin = this.columns().reduce(
                ( win, col ) => win || sum0( col ) == 0,
                false
            );

            return horizontalWin || verticalWin;
        }
    };
};


function sum0( numbers ) {
    return numbers.reduce( ( a, b ) => a + ( b || 0 ), 0 );
}

function defined( x ) {
    return typeof x !== typeof undefined;
}

function cloneBoard( board ) {
    return board.map( row => row.map( x => x ) );
}
