"use strict";

const { defined } = require( '../lib/utils' );

exports.part1 = function ( paths ) {
    const mesh = Mesh( paths );

    const routes = mesh.walk( 'start', 'end', true );

    return routes.length;
}


exports.part2 = function ( paths ) {
    const mesh = Mesh( paths );

    const routes = mesh.walk( 'start', 'end' );

    return routes.length;
}


function Mesh( paths ) {
   const mesh = {};

   paths.forEach(
        ( [ from, to ] ) => {
            mesh[ from ] = mesh[ from ] || Cave();
            mesh[ to ] = mesh[ to ] || Cave();
            mesh[ from ].addNeighbour( to );
            mesh[ to ].addNeighbour( from );
        }
    );

    function walkMesh ( from, to, doubleVisited = false, path = [], seen = {}, neverRevisit = { 'start': 1 } ) {
        if( seen[ from ] ) {
            if( ! doubleVisited && ! defined( neverRevisit[ from ] ) ) {
                doubleVisited = true;
            } else {
                throw 'invalid route';
            }
        }

        seen[ from ] = from.toLowerCase() == from;

        let routes = [];

        if( from == to ) {
            return [ [ ...path, from ] ];
        }

        mesh[ from ].neighbours().forEach(
            ( neighbour ) => {
                try {
                    routes = routes.concat(
                        walkMesh( neighbour, to, doubleVisited, [ ...path, from ], {...seen}, {...neverRevisit} )
                    );
                } catch ( e ) {
                }
            }
        );

        return routes;
    }

    return {
        walk: walkMesh
    };
}


function Cave() {
    const neighbours = [];

    return {
        addNeighbour: ( neighbour ) => neighbours.push( neighbour ),
        neighbours: () => [...neighbours]
    };
}
