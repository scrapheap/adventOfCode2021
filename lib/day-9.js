"use strict";
const { sum0, product } = require( '../lib/utils' );

exports.part1 = function ( heights ) {
    const lowPoints = [];

    const maxX = heights.length;
    const maxY = heights[0].length;

    const neighbours = neighbourFinder( heights );

    for( let x = 0; x < maxX; x++ ) {
        for( let y = 0; y < maxY; y++ ) {
            if( Math.min( ...neighbours( x, y ) ) > heights[ x ][ y ] ) {
                lowPoints.push( heights[ x ][ y ] );
            }
        }
    }

    return sum0( lowPoints.map( x => x + 1 ) );
}


exports.part2 = function ( heights ) {
    const lowPoints = [];

    const maxX = heights.length;
    const maxY = heights[0].length;

    const neighbours = neighbourFinder( heights );

    for( let x = 0; x < maxX; x++ ) {
        for( let y = 0; y < maxY; y++ ) {
            if( Math.min( ...neighbours( x, y ) ) > heights[ x ][ y ] ) {
                lowPoints.push( { x, y } );
            }
        }
    }

    const walkBasin = basinWalker( heights );

    const basins = lowPoints.map( walkBasin );

    const largest3 = basins.sort( ( a, b ) => a.length - b.length ).slice( -3 );

    return product( largest3.map( x => x.length ) );
}

function neighbourFinder( heights ) { 
    const edgeX = heights.length - 1;
    const edgeY = heights[0].length - 1;

    return function ( x, y ) {
        const neighbours = [];

        if( x )  {
            neighbours.push( heights[ x - 1 ][ y ] );
        }

        if( y )  {
            neighbours.push( heights[ x ][ y - 1 ] );
        }

        if( x < edgeX ) {
            neighbours.push( heights[ x + 1 ][ y ] );
        }

        if( y < edgeY )  {
            neighbours.push( heights[ x ][ y + 1 ] );
        }

        return neighbours;
    };
}

function basinWalker( heights ) {
    const edge = { x: heights.length - 1, y: heights[0].length - 1 };

    return function ( loc ) {
        const basin = {};
        const neighbours = [ loc ];

        while( neighbours.length ) {
            const { x, y } = neighbours.pop();

            if( heights[x][y] != 9 ) {
                basin[ JSON.stringify( { x, y } ) ] = true;

                if ( x  &&  ! basin[ JSON.stringify( { x: x - 1,  y } ) ] ) {
                    neighbours.push( { x: x - 1,  y } );
                }
                    
                if ( y  &&  ! basin[ JSON.stringify( { x,  y: y - 1 } ) ] ) {
                    neighbours.push( { x,  y: y - 1 } );
                }
                    
                if ( x < edge.x  &&  ! basin[ JSON.stringify( { x: x + 1,  y } ) ] ) {
                    neighbours.push( { x: x + 1,  y } );
                }
                    
                if ( y < edge.y  &&  ! basin[ JSON.stringify( { x,  y: y + 1 } ) ] ) {
                    neighbours.push( { x,  y: y + 1 } );
                }
            }
        }

        return Object.keys( basin ).map( loc => JSON.parse( loc ) );
    };
}
