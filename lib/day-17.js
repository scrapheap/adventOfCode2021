"use strict";

const { defined } = require( '../lib/utils' );

exports.part1 = function ( target ) {
    const trench = Area( target );

    const maxX = Math.abs( target[1][0] );
    const maxY = Math.abs( target[0][1] );

    let heights = [];

    for( let x = 0; x <= maxX; x++ ) {
        for( let y = 0; y <= maxY; y++) {
            const height = testProbe( x, y, trench );

            if( defined( height ) ) {
                heights.push( height );
            }

        }
    }

    return Math.max( ...heights );
}


exports.part2 = function ( target ) {
    const trench = Area( target );

    const maxX = Math.abs( target[1][0] );
    const maxY = Math.abs( target[0][1] );

    let valid = [];

    for( let x = -maxX; x <= maxX; x++ ) {
        for( let y = -maxY; y <= maxY; y++) {
            const height = testProbe( x, y, trench );

            if( defined( height ) ) {
                valid.push( [ x, y ] );
            }

        }
    }

    return valid.length;
}


function testProbe ( x, y, trench ) {
    let maxHeight = 0;

    let curX = 0;
    let curY = 0;

    while ( ! trench.within( curX, curY ) && ! trench.beyond( curX, curY ) ) {
        maxHeight = Math.max( maxHeight, curY );

        curX += x;
        curY += y;

        if( x > 0 ) { x-- };
        if( x < 0 ) { x++ };

        y--;
    }

    return trench.within( curX, curY ) ?  maxHeight  :  undefined;
}


function Area ( [ [ fromX, fromY ], [ toX, toY ] ] ) {
    return {
        within: ( x, y ) =>
            fromX <= x  &&  x <= toX  &&  fromY <= y  &&  y <= toY,
        beyond: ( x, y ) =>
            x > toX  ||  y < fromY
    };
};
