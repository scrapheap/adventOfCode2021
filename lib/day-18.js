"use strict";

const { defined } = require( '../lib/utils' );
const SnailfishNumber = require( '../lib/SnailfishNumber.js');

exports.part1 = function ( numbers ) {
    let snailfishNumbers = numbers.map( ( sfn ) => SnailfishNumber.new( sfn ) );

    let finalSfn = snailfishNumbers.reduce( ( a, b ) => SnailfishNumber.add( a, b ) );

    return finalSfn.magnitude();
}


exports.part2 = function ( numbers ) {
    let largestMagnitude = 0;
    for( let x = 0; x < numbers.length; x++ ) {
        for( let y = 0; y < numbers.length; y++ ) {
            let sfn1 = SnailfishNumber.new( numbers[x] );
            let sfn2 = SnailfishNumber.new( numbers[y] );

            largestMagnitude = Math.max( largestMagnitude, SnailfishNumber.add( sfn1, sfn2 ).magnitude() );
        }
    }

    return largestMagnitude;
}
