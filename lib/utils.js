exports.sum0    = ( numbers )  =>  numbers.reduce( ( a, b ) => a + ( b || 0 ), 0 );
exports.product = ( numbers )  =>  numbers.reduce( ( a, b ) => a * b, 1 );

exports.defined = ( x )        =>  typeof x !== typeof undefined;

exports.flat    = ( arr )      =>  [].concat(...arr);

exports.uniq    =
    ( arr ) => {
        const vals = {};
        arr.forEach( ( val ) => vals[ val ] = 1);
        return Object.keys( vals );
    };
