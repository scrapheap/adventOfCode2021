"use strict";
const { sum0, product } = require( '../lib/utils' );

exports.part1 = function ( lines ) {

    const charPoints = {
        ")": 3,
        "]": 57,
        "}": 1197,
        ">": 25137
    };

    const chunkErrors = [];

    lines.forEach( ( line ) => {
        try {
            parseLine( line );
        } catch ( errChar ) {
            chunkErrors.push( errChar )
        }
    } )

    return sum0( chunkErrors.map( (errChar) => charPoints[ errChar ] ) );
}


exports.part2 = function ( lines ) {
    const closings = [];

    lines.forEach( ( line ) => {
        try {
            closings.push( parseLine( line ).reverse() );
        } catch ( errChar ) {
            // Discard invalid lines
        }
    } );

    const scores = closings.map( scoreRequiredClosings );

    scores.sort( ( a, b ) => a - b );

    return scores[ Math.floor( scores.length / 2 ) ];
}


function parseLine( line ) {
    const open = [];

    line.split('').forEach(
        ( ch ) => {
            if ( ch.match( /[\[\<\{\(]/ ) ) {
                open.push( ch );
            } else {
                if ( ! validPair( open.pop(), ch ) ) {
                    throw ch;
                }
            }
        }
    )

    return open;
}


function validPair( o, c ) {
    return o == "[" && c == "]"  ||  o =="(" && c ==")"  ||  o =="{" && c =="}"  || o == "<" && c == ">";
}


function scoreRequiredClosings( closings ) {
    const charPoints = {
        "(": 1,
        "[": 2,
        "{": 3,
        "<": 4
    };

    return closings.reduce( ( score, ch ) => ( score * 5 ) + charPoints[ ch ], 0 );
}
