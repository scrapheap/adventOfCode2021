exports.new = function ( srcCells ) {
    const cells = srcCells.map( ( row ) => [...row] );

    let maxY = cells.length - 1;
    let maxX = cells[0].length - 1;

    return {
        forEach: ( callback ) => {
            for( let y = 0; y <= maxY; y++ ) {
                for( let x = 0; x <= maxX; x++ ) {
                    callback( x, y, cells[y][x] );
                }
            }
        },
        map: ( callback ) => {
            const newCells = [];

            for( let y = 0; y <= maxY; y++ ) {
                newCells[y] = [];
                for( let x = 0; x <= maxX; x++ ) {
                    newCells[y][x] = callback( x, y, cells[y][x] );
                }
            }
            return exports.new( newCells );
        },
        neighbours: ( x, y ) => {
            const nbors = [];

            for( _y = Math.max( y - 1, 0 ); _y <= Math.min( y + 1, maxY ); _y++ ) {
                for( _x = Math.max( x - 1, 0 ); _x <= Math.min( x + 1, maxX ); _x++ ) {
                    if( _x != x  ||  _y != y ) {
                        nbors.push( cells[ _y ][ _x ] );
                    }
                }
            }

            return nbors;
        },
        axialNeighbours: ( x, y ) => {
            const nbors = [];
            if( x - 1 > 0 )  {  nbors.push( cells[ x - 1 ][ y ] );  }
            if( x < maxX )   {  nbors.push( cells[ x + 1 ][ y ] );  }
            if( y - 1 > 0 )  {  nbors.push( cells[ x ][ y - 1 ] );  }
            if( y < maxY )   {  nbors.push( cells[ x ][ y + 1 ] );  }

            return nbors;
        },
        set: ( x, y, value ) => {
            return cells[x][y] = value;
        },
        get: ( x, y ) => {
            return cells[x][y];
        },
        dump: () => {
            console.log( cells );
        },
        pad: ( padding ) => {
            cells.forEach( ( r ) => {
                r.unshift( padding );
                r.push( padding );
            } );

            cells.unshift( cells[0].map( ( a ) => padding ) );
            cells.push( cells[0].map( ( a ) => padding ) );

            maxX += 2;
            maxY += 2;
        },

        chunk: ( fromX, fromY, toX, toY ) => {
            const cnk = [];

            for( y = fromY; y <= toY; y++ ) {
                for( x = fromX; x <= toX; x++) {
                    if ( x < 0 || y < 0 || x > maxX || y > maxY ) {
                        cnk.push( undefined );
                    } else {
                        cnk.push( cells[y][x] );
                    }
                }
            }

            return cnk;
        }
    }
};
