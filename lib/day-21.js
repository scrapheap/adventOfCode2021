"use strict";

exports.part1 = function ( starts ) {
    const die = newDie();

    const scores = starts.map( () => 0 );
    const positions = starts.map( ( x ) => x - 1);

    let currentPlayer = 0;

    while( Math.max( ...scores ) < 1000 ) {
        const distance = die.roll() + die.roll() + die.roll();

        positions[ currentPlayer ] = ( positions[ currentPlayer ] + distance ) % 10;

        scores[ currentPlayer ] += positions[ currentPlayer ] + 1;

        currentPlayer = ( currentPlayer + 1 ) % positions.length;
    }

    return Math.min( ...scores ) * die.rolled();
}


exports.part2 = function ( starts ) {
}

function newDie() {
    let currentValue = 0;

    let rolled = 0;

    return {
        roll: () => {
            rolled++;
            currentValue = ( currentValue + 1 ) % 100;

            return currentValue;
        },

        rolled: () => rolled,
    };
}
