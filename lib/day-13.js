"use strict";

const { defined } = require( '../lib/utils' );

const fold = {
    'y': ( offset, points ) => {
        const newPoints = points.map( ( [ x, y ] ) => [
            x,
            y > offset
                ? offset - ( y - offset )
                : y
            ]
        );

        return uniquePoints( newPoints );
    },

    'x': ( offset, points ) => {
        const newPoints = points.map( ( [ x, y ] ) => [
            x > offset
                ? offset - ( x - offset )
                : x,
            y
            ]
        );

        return uniquePoints( newPoints );
    }
}

exports.part1 = function ( transparency ) {
    const [ direction, offset ] = transparency.folds[0];

    const pointsAfterFold1 = fold[ direction ]( offset, transparency.points );

    return pointsAfterFold1.length;
}


exports.part2 = function ( transparency ) {
    const finalPoints = transparency.folds.reduce(
        ( points, [ direction, offset ] ) => fold[ direction ]( offset, points ),
        transparency.points
    );

    const maxY = Math.max( ...finalPoints.map( ( [x, y] ) => y ) );
    const maxX = Math.max( ...finalPoints.map( ( [x, y] ) => x ) );

    const grid = [];

    for( let y = 0; y <= maxY; y++ ) {
        grid[y]=[];
        for( let x = 0; x <= maxX; x++ ) {
            grid[y][x] = '.';
        }
    }

    finalPoints.forEach(
        ( [ x, y ] ) => grid[ y ][ x ] = '#'
    );

    const stringOutput = grid
        .map(
            ( row ) =>
                row
                    .map( ( v ) => defined( v ) ? v : '.' )
                    .join( '' )
        )
        .join( "\n" );

    return "\n" + stringOutput;
}

function uniquePoints ( points ) {
    const uniqPoints = points.reduce(
        ( uniq, point ) => {
            uniq[ point.join(',')] = point;
            return uniq;
        },
        {}
    );

    return Object.values( uniqPoints );
}
