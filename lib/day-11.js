"use strict";
const Grid = require( '../lib/Grid' );

exports.part1 = function ( scan ) {
    const octopusses = Grid.new( scan.map( ( row ) => row.map( octopuss ) ) );

    octopusses.forEach(
        ( x, y, octopuss ) => {
            octopuss.setNeighbours( octopusses.neighbours( x, y ) );
        }
    );

    let flashCount = 0;

    for( let step = 0; step < 100; step++ ) {
        octopusses.forEach(
            ( x, y, octopuss ) => octopuss.trigger()
        );

        octopusses.forEach(
            ( x, y, octopuss ) => octopuss.step( ( flashed ) => { if ( flashed ) { flashCount++ } } )
        );
    }

    return flashCount;
}


exports.part2 = function ( scan ) {
    const octopusses = Grid.new( scan.map( ( row ) => row.map( octopuss ) ) );

    octopusses.forEach(
        ( x, y, octopuss ) => {
            octopuss.setNeighbours( octopusses.neighbours( x, y ) );
        }
    );

    let step = 0;
    let allFlashed = false;

    while( ! allFlashed ) {
        step++;

        octopusses.forEach(
            ( x, y, octopuss ) => octopuss.trigger()
        );

        allFlashed = true;

        octopusses.forEach(
            ( x, y, octopuss ) => octopuss.step( ( flashed ) => allFlashed = flashed && allFlashed )
        );
    }

    return step;
}


function octopuss( initialValue ) {
    let value = initialValue;
    let neighbours = [];
    let flashed = false;

    return {
        trigger:
            () => {
                value++;
                if ( value > 9 && ! flashed ) {
                    flashed = true;
                    neighbours.forEach(
                        ( neighbour ) => neighbour.trigger()
                    );
                }
            },

        setNeighbours:
            ( octopusses ) => neighbours = octopusses,

        step:
            ( callback ) => {
                callback( flashed );
                if( flashed ) {
                    value = 0;
                    flashed = false;
                }
            }
    };
}
