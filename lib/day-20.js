"use strict";

const { defined } = require( '../lib/utils' );
const Grid = require( '../lib/Grid.js' );

exports.part1 = function ( { algorithm, image } ) {
    let enhanced = algorithm.split( '' ).map( toBin );

    let pixels = Grid.new( image.map( a => a.split('').map( toBin ) ) );

    let infinitePixel = 0;

    for( let step = 0; step < 2; step++ ) {
        pixels.pad( infinitePixel );
        pixels = pixels.map( (x, y, v ) => {
            const chunk = pixels.chunk( x - 1, y - 1, x + 1, y + 1 );
            const offset = parseInt( chunk.map( ( a ) => defined(a) ? a : infinitePixel ).join(''), 2 );
            return enhanced[ offset ];
        } );

        infinitePixel = infinitePixel ? enhanced[ 511 ] : enhanced[ 0 ];
    }

    let total = 0;
    pixels.forEach( ( x, y, v ) => total += v );

    return total;
}


exports.part2 = function ( { algorithm, image } ) {
    let enhanced = algorithm.split( '' ).map( toBin );

    let pixels = Grid.new( image.map( a => a.split('').map( toBin ) ) );

    let infinitePixel = 0;

    for( let step = 0; step < 50; step++ ) {
        pixels.pad( infinitePixel );
        pixels = pixels.map( (x, y, v ) => {
            const chunk = pixels.chunk( x - 1, y - 1, x + 1, y + 1 );
            const offset = parseInt( chunk.map( ( a ) => defined(a) ? a : infinitePixel ).join(''), 2 );
            return enhanced[ offset ];
        } );

        infinitePixel = infinitePixel ? enhanced[ 511 ] : enhanced[ 0 ];
    }

    let total = 0;
    pixels.forEach( ( x, y, v ) => total += v );

    return total;
}


function toBin( a ) {
    return a == "#" ? 1 : 0;
}
