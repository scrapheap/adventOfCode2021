const { sum0, product } = require( '../lib/utils' );

const hexToBin = {
    "0": "0000",
    "1": "0001",
    "2": "0010",
    "3": "0011",
    "4": "0100",
    "5": "0101",
    "6": "0110",
    "7": "0111",
    "8": "1000",
    "9": "1001",
    "A": "1010",
    "B": "1011",
    "C": "1100",
    "D": "1101",
    "E": "1110",
    "F": "1111"
};

exports.new = function ( hexStream ) {
    const bitStream = hexStream.split('').reduce(
        ( bitStream, hex ) => bitStream + hexToBin[ hex.toUpperCase() ],
        ""
    );

    const packets = parseBitStream( bitStream );

    return {
        packets
    };
};


function parseBitStream( bitStream ) {
    const packets = [];

    let packet = undefined;

    do {
        [ packet, bitStream ] = readPacket( bitStream );
        packets.push( packet );
    } while( bitStream.length > 3 );

    return packets;
}

function readPacket( bitStream ) {
    const packet = {};

    packet.version  =  parseInt( bitStream.slice(0,3), 2 );
    packet.type     =  parseInt( bitStream.slice(3,6), 2 );
    packet.value    =  undefined;
    packet.packets  =  [];

    bitStream = bitStream.slice( 6 );

    // literal
    if ( packet.type == 4 ) {
        let value = undefined;

        [ value, bitStream ] = readLiteral( bitStream );

        packet.value = value;

    } else {

        // Operator
        const type = bitStream[0];
        bitStream = bitStream.slice( 1 );

        if ( type == "0" ) {
            // length defined
            const length = parseInt( bitStream.slice( 0, 15 ), 2 );
            bitStream = bitStream.slice( 15 );

            let subBitStream = bitStream.slice( 0, length );
            bitStream = bitStream.slice( length );

            while ( subBitStream.length ) {
                let _packet = undefined;

                [ _packet, subBitStream ] = readPacket( subBitStream );

                packet.packets.push( _packet )
            }

        } else {
            // count defnied
            let count = parseInt( bitStream.slice( 0, 11 ), 2 );
            bitStream = bitStream.slice( 11 );

            while( count-- ) {
                let _packet = undefined;

                [ _packet, bitStream ] = readPacket( bitStream );

                packet.packets.push( _packet );
            }
        }
    }

    // calculate packet value
    switch ( packet.type ) {
        case 0: packet.value = sum0( packet.packets.map( ( p ) => p.value ) );
            break;
        case 1: packet.value = product( packet.packets.map( ( p ) => p.value ) );
            break;
        case 2: packet.value = Math.min( ... packet.packets.map( ( p ) => p.value ) );
            break;
        case 3: packet.value = Math.max( ... packet.packets.map( ( p ) => p.value ) );
            break;

        // Ignore packet type 4 as literals have already been processed

        case 5: packet.value = ( packet.packets[0].value > packet.packets[1].value ) ? 1 : 0;
            break;
        case 6: packet.value = ( packet.packets[0].value < packet.packets[1].value ) ? 1 : 0;
            break;
        case 7: packet.value = ( packet.packets[0].value == packet.packets[1].value ) ? 1 : 0;
            break;
    };

    return [ packet, bitStream ];
}

function readLiteral( bitStream ) {
    let chunks = "";
    let more = true;

    while( more ) {
        const chunk = bitStream.slice( 0, 5 );
        bitStream = bitStream.slice( 5 );

        chunks += chunk.slice( 1 );
        more = chunk[0] == "1";
    }

    return [ parseInt( chunks, 2 ), bitStream ];
}
