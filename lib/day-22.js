"use strict";

const { defined, sum0 } = require( '../lib/utils' );

exports.part1 = function ( instructions ) {
    const reactor = {};

    instructions.forEach(
        ( [ op, x1, x2, y1, y2, z1, z2 ] ) => {
            overLimitedRange( x1, x2, y1, y2, z1, z2,
                ( x, y, z ) => {
                    reactor[ `${x},${y},${z}` ] = op == "on";
                }
            );
        }
    );

    const active = sum0( Object.values( reactor ) );

    return active;
}


function overLimitedRange( x1, x2, y1, y2, z1, z2, callback ) {
    const fromX = Math.min( x1, x2 );
    const fromY = Math.min( y1, y2 );
    const fromZ = Math.min( z1, z2 );

    const toX = Math.max( x1, x2 );
    const toY = Math.max( y1, y2 );
    const toZ = Math.max( z1, z2 );

    if( within( fromX, toX, -50, 50 ) &&
        within( fromY, toY, -50, 50 ) &&
        within( fromZ, toZ, -50, 50 ) ) {

        for( let x = fromX; x <= toX; x++ ) {
            for( let y = fromY; y <= toY; y++ ) {
                for( let z = fromZ; z <= toZ; z++ ) {
                    callback( x, y, z );
                }
            }
        }
    }
}

function within( from, to, lower, upper ) {
    return ( lower <= to && to <= upper ) || ( lower <= from && from <= upper ) || ( lower >= from && upper <= to );
}


exports.part2 = function ( inst ) {
    const instructions = inst.map( ( x ) => normalise( x ) );

    const activeCubes = [];

    instructions.forEach(
        ( [ op, x1, x2, y1, y2, z1, z2 ] ) => {
            const cube = newCube( x1, x2, y1, y2, z1, z2 );

            activeCubes.forEach(
                ( aCube ) => aCube.removeChunk( x1, x2, y1, y2, z1, z2 )
            );

            if( op == "on" ) {
                activeCubes.push( cube );
            }
        }
    );

    return sum0( activeCubes.map( ( x ) => x.volume() ) );
}


function newCube ( fromX, toX, fromY, toY, fromZ, toZ ) {
    const chopped = [];

    return {
        volume: () => {
            return volume( fromX, toX, fromY, toY, fromZ, toZ ) - sum0( chopped.map( ( c ) => c.volume() ) );
        },
        removeChunk: ( x1, x2, y1, y2, z1, z2 ) => {
            if( within( x1, x2, fromX, toX ) && 
                within( y1, y2, fromY, toY ) && 
                within( z1, z2, fromZ, toZ ) ) {
                
                chopped.forEach(
                    ( cCube ) => cCube.removeChunk( x1, x2, y1, y2, z1, z2 )
                );

                chopped.push(
                    newCube(
                        Math.max( fromX, x1 ), Math.min( toX, x2 ),
                        Math.max( fromY, y1 ), Math.min( toY, y2 ),
                        Math.max( fromZ, z1 ), Math.min( toZ, z2 )
                    )
                );
            }
        }
    };
}

// makes sure our operation's cube dimensions are in a sensible order
function normalise( [ op, x1, x2, y1, y2, z1, z2 ] ) {
    return [
        op,
        Math.min( x1, x2 ), Math.max( x1, x2 ),
        Math.min( y1, y2 ), Math.max( y1, y2 ),
        Math.min( z1, z2 ), Math.max( z1, z2 )
    ];
}

// Calculate the volume, the +1's are to include of the final part of the dimension
function volume ( x1, x2, y1, y2, z1, z2 ) {
    return ( Math.abs( x2 - x1 ) + 1 ) * ( Math.abs( y2 - y1 ) + 1 ) * ( Math.abs( z2 - z1 ) + 1 );
}
