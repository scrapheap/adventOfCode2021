"use strict";

const { uniq, defined } = require( '../lib/utils' );

exports.part1 = function ( polymer ) {
    let template = polymer.template;

    for( let step = 0; step < 10; step++ ) {
        template = template.split('').reduce(
            ( template, c ) => template.length
                ? template + polymer.rules[ template.slice( -1 ) + c ] + c
                : c,
            ""
        );
    }

    const elementCount = {};

    template.split('').forEach(
        ( element ) => elementCount[ element ] = ( elementCount[ element ] || 0 ) + 1
    );

    return Math.max( ...Object.values( elementCount ) ) - Math.min( ...Object.values( elementCount ) );
}


exports.part2 = function ( polymer ) {
    // Naive approach used in part1 doesn't scale to 40 steps, but as we don't
    // require more than counts we can build count the starting pairs and
    // then for each step we can extrapolate the new counts for each pair

    let template = polymer.template;

    let rules = {};
    
    // as we're extrapolating pair counts now we need to know what new pairs
    // are produced for each rule
    Object.keys( polymer.rules ).forEach(
        ( pair ) => {
            rules[ pair ] = [ pair[0] + polymer.rules[ pair ], polymer.rules[ pair ] + pair[1] ];
        }
    );
    
    // we need to set up our pair counts before we can extrapolote
    let pairs = {};

    for( let i = 0; i < template.length - 1; i++ ) {
        const pair = template.slice( i, i + 2 );
        pairs[ pair ] = ( pairs[ pair ] || 0 ) + 1;
    }

    // for each step we can now just create a new set of pair counts by
    // extrapolating from the previous set of pair counts
    for( let step = 0; step < 40; step++ ) {
        const newPairs = {};

        Object.keys( pairs ).forEach(
            ( pair ) => {
                rules[ pair ].forEach(
                    ( newPair )  =>  newPairs[ newPair ] = ( newPairs[ newPair ] || 0 ) + pairs[ pair ]
                );
            }
        )

        pairs = newPairs;
    }

    
    const elementCount = {};

    Object.keys( pairs ).forEach(
        ( pair )  => pair.split('').forEach(
            ( element ) => elementCount[ element ] = ( elementCount[ element ] || 0 ) + ( pairs[ pair ] )
        )
    );

    // We could be off by one here so if the first value is wrong then
    // subtract one from it
    return Math.ceil( ( Math.max( ...Object.values( elementCount ) ) / 2 )  -  ( Math.min( ...Object.values( elementCount ) ) / 2 ) );
}

