exports.part1 = function ( readings ) {
    const bitmap = readings.map( x => x.split( '' ) );

    let columns = [];
    let column = 0;
    bitmap[0].forEach( () => {
        columns.push( bitmap.map( x => x[column] ) );
        column++;
    } );

    const counts = columns.map(
        ( col ) => col.reduce(
            ( a, b ) => {
                a[b]++;
                return a;
            },
            { "0":0, "1":0 }
        )
    );

    const gamma = counts.reduce(
        (a, b) => a + ( b["0"] > b["1"] ? "0" : "1" ),
        ""
    );

    const epsilon = counts.reduce(
        (a, b) => a + ( b["0"] < b["1"] ? "0" : "1" ),
        ""
    );

    return parseInt( gamma, 2 ) * parseInt( epsilon, 2 );
};

exports.part2 = function ( readings ) {
    
    const oxygen = oxygenReading( readings );
    const co2 = co2Reading( readings );

    return oxygen * co2;
};


function oxygenReading( readings ) {
    let pos = 0;

    while ( readings.length > 1 ) {
        target = mostCommon( readings, pos );

        readings = readings.filter( a  =>  a[pos] == target );
        pos++;
    }

    return parseInt( readings[0], 2 );
}

function mostCommon( readings, pos ) {
    const counts = readings.reduce(
        ( a, b ) => {
            a[b[pos]]++;
            return a;
        },
        { '0': 0, '1': 0 }
    );

    return counts['0'] > counts['1'] ? '0' : '1';
}


function co2Reading( readings ) {
    let pos = 0;

    while ( readings.length > 1 ) {
        target = leastCommon( readings, pos );

        readings = readings.filter( a  =>  a[pos] == target );

        pos++;
    }

    return parseInt( readings[0], 2 );
}

function leastCommon( readings, pos ) {
    const counts = readings.reduce(
        ( a, b ) => {
            a[b[pos]]++;
            return a;
        },
        { '0': 0, '1': 1 }
    );

    return counts['0'] < counts['1'] ? '0' : '1';
}
