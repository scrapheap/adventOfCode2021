"use strict";

const { uniq, defined, sum0 } = require( '../lib/utils' );
const BITS = require( '../lib/BITS' );

exports.part1 = function ( hexStream ) {
    const stream = BITS.new( hexStream );

    return sumVersions( stream.packets );
}


exports.part2 = function ( hexStream ) {
    const stream = BITS.new( hexStream );

    return stream.packets[0].value;
}


function sumVersions( packets ) {
    let sum = 0;

    packets.forEach(
        ( packet ) => sum +=  packet.version + sumVersions( packet.packets )
    );

    return sum;
}
