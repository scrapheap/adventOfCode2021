"use strict";

const { defined, sum0 } = require( '../lib/utils' );

exports.part1 = function ( layout ) {
    const { solved, validMoves } = require( '../lib/utils-23' );
    const solve = ( layout, seen = {}, depth = 0 ) => {
        const fp = layout.map( ( x ) => x || " " ).join( '' );

        if ( solved( layout ) ) {
            return 0;
        }

        if ( seen[ fp ] ) {
            return  seen[ fp ];
        }
        
        const ec = validMoves( layout ).map(
            ( [ f, t, d ] ) => {
                const nl = [...layout];

                const type = nl[ f ];
                nl[ t ] = nl[ f ];
                nl[ f ] = undefined;

                return ( costs[ type ] * d ) + solve( nl, seen, depth + 1 );
            }
        );

        seen[ fp ] = Math.min( ...ec ); 
        return seen[ fp ];
    };

    const lo = layout.map( ( x ) => x || undefined );

    const energies = solve( lo );

    return Math.min( energies );
}

const costs = {
    "A": 1,
    "B": 10,
    "C": 100,
    "D": 1000,
};



exports.part2 = function ( layout ) {
    const { solved, validMoves } = require( '../lib/utils-23-2' );
    const solve = ( layout, seen = {}, depth = 0 ) => {
        const fp = layout.map( ( x ) => x || " " ).join( '' );

        if ( solved( layout ) ) {
            return 0;
        }

        if ( seen[ fp ] ) {
            return  seen[ fp ];
        }
        
        const ec = validMoves( layout ).map(
            ( [ f, t, d ] ) => {
                const nl = [...layout];
                const type = nl[ f ];
                nl[ t ] = nl[ f ];
                nl[ f ] = undefined;

                return ( costs[ type ] * d ) + solve( nl, seen, depth + 1 );
            }
        );

        seen[ fp ] = Math.min( ...ec ); 
        return seen[ fp ];
    };

    const lo = [
        undefined, undefined, undefined,
        
        layout[3], "D", "D", layout[4],
        undefined, undefined,
        
        layout[7], "C", "B", layout[8],
        undefined, undefined,

        layout[11], "B", "A", layout[12],
        undefined, undefined,

        layout[15], "A", "C", layout[16],
        undefined, undefined,
    ];

    const energies = solve( lo );

    return Math.min( energies );
}

