exports.part1 = function ( route ) {
    const direction = {
        forward: ( pos, x ) => {
            return { h: pos.h + x, d: pos.d };
        },
        down: ( pos, x ) => {
            return { h: pos.h, d: pos.d + x };
        },
        up: ( pos, x ) => {
            return { h: pos.h, d: pos.d - x };
        }
    };

    const pos = route.reduce(
        ( pos, op ) => direction[op[0]]( pos, op[1] ),
        { h: 0, d: 0 }
    ); 

    return pos.h * pos.d;
};

exports.part2 = function ( route ) {
    const direction = {
        forward: ( pos, x ) => {
            return { h: pos.h + x, d: pos.d + ( x * pos.aim ), aim: pos.aim };
        },
        down: ( pos, x ) => {
            return { h: pos.h, d: pos.d, aim: pos.aim + x };
        },
        up: ( pos, x ) => {
            return { h: pos.h, d: pos.d, aim: pos.aim - x };
        }
    };

    const pos = route.reduce(
        ( pos, op ) => direction[op[0]]( pos, op[1] ),
        { aim: 0, h: 0, d: 0 }
    ); 

    return pos.h * pos.d;
};
