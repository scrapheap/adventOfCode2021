exports.part1 = function ( lanternFish ) {
    let school = [ ...lanternFish ];
    for( let day = 0; day < 80; day++) {
        const newFish = school.filter( c => c == 0 ).map( () => 8 );
        school = school
            .map( c  =>  c == 0  ?  6  :  c - 1 )
            .concat( newFish );
    }

    return school.length;
}

exports.part2 = function ( lanternFish ) {
    let school = [ 0, 0, 0, 0, 0, 0, 0, 0, 0 ];
    
    lanternFish.forEach( fish => school[fish]++ );

    for( let day = 0; day < 256; day++ ) {
        school.push( school[0] );
        school[7] += school[0];
        school.shift();
    }

    return sum0( school );
}

function sum0( numbers ) {
    return numbers.reduce( ( a, b ) => a + ( b || 0 ), 0 );
}

function schoolDump( school ) {
    for( let day = 0; day <9; day++ ) {
        console.log( `${day} - ${school[day]}` );
    }
}
