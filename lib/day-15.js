"use strict";

const { uniq, defined } = require( '../lib/utils' );
const Grid = require( '../lib/Grid' );

exports.part1 = function ( map ) {
    const caveRisks = Grid.new( map );
    const totalRisks = caveRisks.map( () => 99999999 );

    const maxX = map.length - 1 ;
    const maxY = map[0].length - 1;

    totalRisks.set( maxX, maxY, caveRisks.get( maxX, maxY ) );

    let changes = 0;
    do {
        changes = 0;
        totalRisks.forEach(
            ( x, y, value ) => {
                if ( x == maxX && y == maxY ) {
                    return;
                }

                const newValue = Math.min( ...totalRisks.axialNeighbours( x, y ) ) + caveRisks.get( x, y );

                if( value != newValue ) {
                    changes++;
                    totalRisks.set( x, y, newValue );
                }
            }
        );
    } while ( changes );

    return totalRisks.get( 0, 0 ) - caveRisks.get( 0, 0 );
}


exports.part2 = function ( map ) {
    const newMap = map.map(  ( row ) => [ ...row ]  );

    const height = map.length;
    const width = map[0].length;

    // populate the new risk values across the map
    newMap.forEach(
        ( row, x ) => {
            row.forEach(
                ( col, y ) => {
                    let risk = col;

                    for( let nY = 1; nY < 5; nY++ ) {
                        risk = incRisk( risk );
                        newMap[x][ ( nY * width ) + y ] = risk;
                    }
                }
            )
        }
    );

    // then we can populate the new risk values down the map
    newMap.forEach(
        ( row, x ) => {

            // Make sure we have the additional rows available to us
            for( let nX = 1; nX < 5; nX++ ) {
                newMap[ (nX * height ) + x ] = [];
            }

            // populate those new rows
            row.forEach(
                ( col, y ) => {
                    let risk = col;

                    for( let nX = 1; nX < 5; nX++ ) {
                        risk = incRisk( risk );
                        newMap[ ( nX * height ) + x ][y] = risk;
                    }
                }
            )
        }
    );

    // Now we've got our new map it's the same logic as part 1 to solve
    return exports.part1( newMap );
}


function incRisk( risk ) {
    const newRisk = risk + 1;

    return  newRisk > 9  ?  1  :  newRisk;
}
