"use strict";

const { defined } = require( '../lib/utils' );

function newSnailfishNumber( param ) {
    let value = undefined;
    let left = undefined;
    let right = undefined;

    if( Array.isArray( param ) ) {
        left = newSnailfishNumber( param[0] );
        right = newSnailfishNumber( param[1] );

    } else {
        value = param;
    }

    return {
        magnitude: () => {
            return defined( value )  ?  value  :  ( left.magnitude() * 3 ) + ( right.magnitude() * 2 );
        },

        split: () => {
            if( ! defined( value ) ) {
                return left.split() || right.split();
            }
            
            if ( value > 9 ) {
                left = newSnailfishNumber( Math.floor( value / 2 ) );
                right = newSnailfishNumber( Math.ceil( value / 2 ) );
                value = undefined;
                return true;
            }

            return false;
        },

        explode: ( depth = 1, pLeft = undefined, pRight = undefined ) => {
            if( defined( value ) ) {
                return false;
            }

            if( depth > 4  &&  left.isValue  &&  right.isValue ) {
                if( defined( pLeft ) ) {
                    pLeft.addToRight( left.value() );
                }

                if( defined( pRight ) ) {
                    pRight.addToLeft( right.value() );
                }

                value = 0;
                left = undefined;
                right = undefined;

                return true;
            }

            return left.explode( depth + 1, pLeft, right ) || right.explode( depth + 1, left, pRight );
        },

        isValue: () => {
            return defined( value );
        },

        value: () => {
            return value;
        },

        add: ( inc ) => {
            value += inc;
        },

        addToRight: ( inc ) => {
            if ( defined( value ) ) {
                value += inc;
                return;
            }

            if ( defined( right ) ) {
                right.addToRight( inc );
            }
        },

        addToLeft: ( inc ) => {
            if ( defined( value ) ) {
                value += inc;
                return;
            }

            if ( defined( left ) ) {
                left.addToLeft( inc );
            }
        },

        toArray: () => {
            return defined( value )  ?  value  :  [ left.toArray(), right.toArray() ];
        }
    }
}

exports.new = newSnailfishNumber;
exports.add = ( a, b ) => {
    let sfn = newSnailfishNumber( [ a.toArray(), b.toArray() ] );

    while( sfn.explode() || sfn.split() );

    return sfn;
}
