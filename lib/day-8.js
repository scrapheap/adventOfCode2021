const { sum0, flat } = require( '../lib/utils' );

exports.part1 = function ( signals ) {
    const digitSignals = flat( signals.map( x => x[1] ) );

    return digitSignals.filter( s => s.length == 2 || s.length == 3 || s.length == 4 || s.length == 7 ).length;
}

exports.part2 = function ( signals ) {
    const readings = signals.map( deduceDigits );
    return sum0( readings );
}

function deduceDigits( digits ) {
    const values = [];
    const assigned = {};

    const fingerprints = {
        "2336": "0",
        "1225": "2",
        "2335": "3",
        "1325": "5",
        "1326": "6",
        "2436": "9",
    };

    const signals = flat( digits ).map( digit => digit.split('').sort().join('') );

    // find 1
    values[1] = signals.filter( segments => segments.length == 2 ).pop();

    // find 4
    values[4] = signals.filter( segments => segments.length == 4 ).pop();

    // find 7
    values[7] = signals.filter( segments => segments.length == 3 ).pop();

    // find 8
    values[8] = signals.filter( segments => segments.length == 7 ).pop();

    const fingerprint = fingerprinter( values[1], values[4], values[7], values[8] );

    Object.keys( fingerprints ).forEach(
        key => {
            values[ fingerprints[ key ] ] = signals.filter( segments => fingerprint( segments ) == key ).pop();
        }
    );

    for( let x = 0; x<10; x++ ){
        assigned[ values[ x ] ] = x;
    }

    const reading = digits[1].map( digit => digit.split('').sort().join('') );
    return parseInt( reading.map( x => assigned[x] ).join(''), 10 );
}

function fingerprinter( _1, _4, _7, _8 ) {
    return function ( segments ) {
        const fingerprint = [
            inCommon( segments, _1 ).toString(),
            inCommon( segments, _4 ).toString(),
            inCommon( segments, _7 ).toString(),
            inCommon( segments, _8 ).toString()
        ];
        
        return fingerprint.join('');
    };
}

function inCommon( a, b ) {
    return a.split('').reduce(
        (acc, segment) => acc + ( b.includes( segment ) ? 1 : 0 ),
        0
    );
}
