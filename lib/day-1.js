exports.part1 = function ( data ) {
    let increases = 0;
    
    data.reduce(
        ( a, b ) => {
            if ( defined( a ) && b > a ) {
                increases++;
            }

            return b;
        },
        undefined
    );

    return increases;
};

exports.part2 = function ( data ) {
    let increases = 0;

    let slidingWindows = [];
    
    data.reduce(
        (a, b) => {
            a.push( b );
            if (a.length > 3 ) {
                a.shift();
            }

            if (a.length == 3 ) {
                slidingWindows.push( sum( a ) );
            }
            
            return a;
        },
        []
    );

    return exports.part1( slidingWindows );
};


function defined( x ) {
    return typeof x !== typeof undefined;
}

function sum( x ) {
    return x.reduce( ( a, b ) => a + b, 0 );
}
